def insertion_sort(num):
    if len(num) <= 1:
        return

    for i in range(1, len(num)):
        key = num[i]
        j = i - 1
        while j >= 0 and num[j] > key :
            num[j+1] = num[j]
            j -= 1
        num[j + 1] = key

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ").split()))
    insertion_sort(numbers)
    print("Sorted number is", numbers)

