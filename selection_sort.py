def selection_sort(list_num):
    long = len(list_num)

    for step in range(long):
        min_idx = step
        for i in range(step+1, long):
            if list_num[i] < list_num[min_idx]:
                min_idx = i
        list_num[step], list_num[min_idx] = list_num[min_idx], list_num[step]

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ").split()))
    selection_sort(numbers)
    print("Sorted number is", numbers)
